# Resumo

O algoritmo de ordenação paralelo Quickmerge combina a estratégia
do algoritmo Quicksort com operações de fusão de subconjuntos criados a par-
tir de elementos chaves, chamados pivôs. Apesar de que existem duas versões
do algoritmo propostas para o hipercubo, nenhuma considera falhas. Este
trabalho apresenta uma implementação MPI tolerante a falhas dos algorit-
mos Quickmerge e Quickmerge Modificado na topologia virtual denominada
VCube. Os algoritmos propostos são capazes de executar a ordenação mesmo
que todos menos um processo falhem. Os algoritmos são comparados a uma
implementação tolerante a falhas do algoritmo paralelo Hyperquicksort. Re-
sultados mostram a eficiência da implementação na ordenação de até 1 bilhão
de números em cenários com e sem falhas.

# Modelo de sistema e definições

Os processos estão organizados em uma topologia de hipercubo virtual denominada VCube (Duarte et al. 2014).Quando todos os processos estão sem falhas o VCube corresponde a um hipercubo.
Os algoritmos Quickmerge, Quickmerge Modificado e Hyperquicksort diferem basicamente na forma como escolhem o número pivô.

# Resultados

A versão tolerante a falhas de cada algoritmo foi implementada na linguagem C
usando a biblioteca ULFM 2.0 (MPI-Forum 2019). A implementação do Hyper-
quicksort utilizada para comparação é a de (Camargo and Duarte, Jr. 2016).
Os experimentos foram executados em uma máquina com 32 processadores Intel
Core i7. O sistema operacional é o Linux Kernel 4.4.0. O desempenho dos algoritmos foi
comparado em um cenário sem falhas e com falhas para 4, 8, 16 e
32 processos MPI.
Resultados de desempenho mostram a eficiência dos algoritmos para ordenar 1073741824 números inteiros em cenários com falhas e sem falhas. O Hy-
perquicksort obteve o melhor desempenho, a diferença de desempenho entre o Quickmerge
Modificado e o Hyperquicksort é pequena. O Quickmerge Modificado obteve o melhor
balanceamento. Nos experimentos com falhas os algoritmos se assemelham em tempo de
execução conforme aumenta o número de falhas.

